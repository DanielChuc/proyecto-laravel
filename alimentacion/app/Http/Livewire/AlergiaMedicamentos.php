<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\AlergiaMedicamento;

class AlergiaMedicamentos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $expediente_id, $nombre;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.alergia-medicamentos.view', [
            'alergiaMedicamentos' => AlergiaMedicamento::latest()
						->orWhere('expediente_id', 'LIKE', $keyWord)
						->orWhere('nombre', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->expediente_id = null;
		$this->nombre = null;
    }

    public function store()
    {
        $this->validate([
		'expediente_id' => 'required',
		'nombre' => 'required',
        ]);

        AlergiaMedicamento::create([ 
			'expediente_id' => $this-> expediente_id,
			'nombre' => $this-> nombre
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'AlergiaMedicamento Successfully created.');
    }

    public function edit($id)
    {
        $record = AlergiaMedicamento::findOrFail($id);

        $this->selected_id = $id; 
		$this->expediente_id = $record-> expediente_id;
		$this->nombre = $record-> nombre;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'expediente_id' => 'required',
		'nombre' => 'required',
        ]);

        if ($this->selected_id) {
			$record = AlergiaMedicamento::find($this->selected_id);
            $record->update([ 
			'expediente_id' => $this-> expediente_id,
			'nombre' => $this-> nombre
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'AlergiaMedicamento Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = AlergiaMedicamento::where('id', $id);
            $record->delete();
        }
    }
}
