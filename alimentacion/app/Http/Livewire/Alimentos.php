<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Alimento;

class Alimentos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $nombre, $tipoalimento_id, $descripcion, $ingredientes, $preparacion, $calorias, $urlvideo, $urlfotos;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.alimentos.view', [
            'alimentos' => Alimento::latest()
						->orWhere('nombre', 'LIKE', $keyWord)
						->orWhere('tipoalimento_id', 'LIKE', $keyWord)
						->orWhere('descripcion', 'LIKE', $keyWord)
						->orWhere('ingredientes', 'LIKE', $keyWord)
						->orWhere('preparacion', 'LIKE', $keyWord)
						->orWhere('calorias', 'LIKE', $keyWord)
						->orWhere('urlvideo', 'LIKE', $keyWord)
						->orWhere('urlfotos', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->nombre = null;
		$this->tipoalimento_id = null;
		$this->descripcion = null;
		$this->ingredientes = null;
		$this->preparacion = null;
		$this->calorias = null;
		$this->urlvideo = null;
		$this->urlfotos = null;
    }

    public function store()
    {
        $this->validate([
		'nombre' => 'required',
		'tipoalimento_id' => 'required',
		'calorias' => 'required',
        ]);

        Alimento::create([ 
			'nombre' => $this-> nombre,
			'tipoalimento_id' => $this-> tipoalimento_id,
			'descripcion' => $this-> descripcion,
			'ingredientes' => $this-> ingredientes,
			'preparacion' => $this-> preparacion,
			'calorias' => $this-> calorias,
			'urlvideo' => $this-> urlvideo,
			'urlfotos' => $this-> urlfotos
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'Alimento Successfully created.');
    }

    public function edit($id)
    {
        $record = Alimento::findOrFail($id);

        $this->selected_id = $id; 
		$this->nombre = $record-> nombre;
		$this->tipoalimento_id = $record-> tipoalimento_id;
		$this->descripcion = $record-> descripcion;
		$this->ingredientes = $record-> ingredientes;
		$this->preparacion = $record-> preparacion;
		$this->calorias = $record-> calorias;
		$this->urlvideo = $record-> urlvideo;
		$this->urlfotos = $record-> urlfotos;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'nombre' => 'required',
		'tipoalimento_id' => 'required',
		'calorias' => 'required',
        ]);

        if ($this->selected_id) {
			$record = Alimento::find($this->selected_id);
            $record->update([ 
			'nombre' => $this-> nombre,
			'tipoalimento_id' => $this-> tipoalimento_id,
			'descripcion' => $this-> descripcion,
			'ingredientes' => $this-> ingredientes,
			'preparacion' => $this-> preparacion,
			'calorias' => $this-> calorias,
			'urlvideo' => $this-> urlvideo,
			'urlfotos' => $this-> urlfotos
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'Alimento Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Alimento::where('id', $id);
            $record->delete();
        }
    }
}
