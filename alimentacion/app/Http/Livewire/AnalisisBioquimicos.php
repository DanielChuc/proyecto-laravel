<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\AnalisisBioquimico;

class AnalisisBioquimicos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $plan_id, $fecha;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.analisis-bioquimicos.view', [
            'analisisBioquimicos' => AnalisisBioquimico::latest()
						->orWhere('plan_id', 'LIKE', $keyWord)
						->orWhere('fecha', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->plan_id = null;
		$this->fecha = null;
    }

    public function store()
    {
        $this->validate([
		'plan_id' => 'required',
		'fecha' => 'required',
        ]);

        AnalisisBioquimico::create([ 
			'plan_id' => $this-> plan_id,
			'fecha' => $this-> fecha
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'AnalisisBioquimico Successfully created.');
    }

    public function edit($id)
    {
        $record = AnalisisBioquimico::findOrFail($id);

        $this->selected_id = $id; 
		$this->plan_id = $record-> plan_id;
		$this->fecha = $record-> fecha;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'plan_id' => 'required',
		'fecha' => 'required',
        ]);

        if ($this->selected_id) {
			$record = AnalisisBioquimico::find($this->selected_id);
            $record->update([ 
			'plan_id' => $this-> plan_id,
			'fecha' => $this-> fecha
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'AnalisisBioquimico Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = AnalisisBioquimico::where('id', $id);
            $record->delete();
        }
    }
}
