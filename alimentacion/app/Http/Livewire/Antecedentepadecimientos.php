<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Antecedentepadecimiento;

class Antecedentepadecimientos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $antecedente_id, $padecimiento_id;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.antecedentepadecimientos.view', [
            'antecedentepadecimientos' => Antecedentepadecimiento::latest()
						->orWhere('antecedente_id', 'LIKE', $keyWord)
						->orWhere('padecimiento_id', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->antecedente_id = null;
		$this->padecimiento_id = null;
    }

    public function store()
    {
        $this->validate([
		'antecedente_id' => 'required',
		'padecimiento_id' => 'required',
        ]);

        Antecedentepadecimiento::create([ 
			'antecedente_id' => $this-> antecedente_id,
			'padecimiento_id' => $this-> padecimiento_id
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'Antecedentepadecimiento Successfully created.');
    }

    public function edit($id)
    {
        $record = Antecedentepadecimiento::findOrFail($id);

        $this->selected_id = $id; 
		$this->antecedente_id = $record-> antecedente_id;
		$this->padecimiento_id = $record-> padecimiento_id;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'antecedente_id' => 'required',
		'padecimiento_id' => 'required',
        ]);

        if ($this->selected_id) {
			$record = Antecedentepadecimiento::find($this->selected_id);
            $record->update([ 
			'antecedente_id' => $this-> antecedente_id,
			'padecimiento_id' => $this-> padecimiento_id
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'Antecedentepadecimiento Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Antecedentepadecimiento::where('id', $id);
            $record->delete();
        }
    }
}
