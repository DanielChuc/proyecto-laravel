<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Antecedente;

class Antecedentes extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $tipo, $nombre, $detalle, $expediente_id;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.antecedentes.view', [
            'antecedentes' => Antecedente::latest()
						->orWhere('tipo', 'LIKE', $keyWord)
						->orWhere('nombre', 'LIKE', $keyWord)
						->orWhere('detalle', 'LIKE', $keyWord)
						->orWhere('expediente_id', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->tipo = null;
		$this->nombre = null;
		$this->detalle = null;
		$this->expediente_id = null;
    }

    public function store()
    {
        $this->validate([
		'tipo' => 'required',
		'nombre' => 'required',
		'detalle' => 'required',
		'expediente_id' => 'required',
        ]);

        Antecedente::create([ 
			'tipo' => $this-> tipo,
			'nombre' => $this-> nombre,
			'detalle' => $this-> detalle,
			'expediente_id' => $this-> expediente_id
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'Antecedente Successfully created.');
    }

    public function edit($id)
    {
        $record = Antecedente::findOrFail($id);

        $this->selected_id = $id; 
		$this->tipo = $record-> tipo;
		$this->nombre = $record-> nombre;
		$this->detalle = $record-> detalle;
		$this->expediente_id = $record-> expediente_id;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'tipo' => 'required',
		'nombre' => 'required',
		'detalle' => 'required',
		'expediente_id' => 'required',
        ]);

        if ($this->selected_id) {
			$record = Antecedente::find($this->selected_id);
            $record->update([ 
			'tipo' => $this-> tipo,
			'nombre' => $this-> nombre,
			'detalle' => $this-> detalle,
			'expediente_id' => $this-> expediente_id
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'Antecedente Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Antecedente::where('id', $id);
            $record->delete();
        }
    }
}
