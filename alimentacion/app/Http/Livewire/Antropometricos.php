<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Antropometrico;

class Antropometricos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $plan_id, $indicador_id, $valor;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.antropometricos.view', [
            'antropometricos' => Antropometrico::latest()
						->orWhere('plan_id', 'LIKE', $keyWord)
						->orWhere('indicador_id', 'LIKE', $keyWord)
						->orWhere('valor', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->plan_id = null;
		$this->indicador_id = null;
		$this->valor = null;
    }

    public function store()
    {
        $this->validate([
		'plan_id' => 'required',
		'indicador_id' => 'required',
		'valor' => 'required',
        ]);

        Antropometrico::create([ 
			'plan_id' => $this-> plan_id,
			'indicador_id' => $this-> indicador_id,
			'valor' => $this-> valor
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'Antropometrico Successfully created.');
    }

    public function edit($id)
    {
        $record = Antropometrico::findOrFail($id);

        $this->selected_id = $id; 
		$this->plan_id = $record-> plan_id;
		$this->indicador_id = $record-> indicador_id;
		$this->valor = $record-> valor;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'plan_id' => 'required',
		'indicador_id' => 'required',
		'valor' => 'required',
        ]);

        if ($this->selected_id) {
			$record = Antropometrico::find($this->selected_id);
            $record->update([ 
			'plan_id' => $this-> plan_id,
			'indicador_id' => $this-> indicador_id,
			'valor' => $this-> valor
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'Antropometrico Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Antropometrico::where('id', $id);
            $record->delete();
        }
    }
}
