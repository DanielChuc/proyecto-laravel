<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\DetalleAnalise;

class DetalleAnalises extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $analisis_id, $indicador, $resultado;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.detalle-analises.view', [
            'detalleAnalises' => DetalleAnalise::latest()
						->orWhere('analisis_id', 'LIKE', $keyWord)
						->orWhere('indicador', 'LIKE', $keyWord)
						->orWhere('resultado', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->analisis_id = null;
		$this->indicador = null;
		$this->resultado = null;
    }

    public function store()
    {
        $this->validate([
		'analisis_id' => 'required',
		'indicador' => 'required',
		'resultado' => 'required',
        ]);

        DetalleAnalise::create([ 
			'analisis_id' => $this-> analisis_id,
			'indicador' => $this-> indicador,
			'resultado' => $this-> resultado
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'DetalleAnalise Successfully created.');
    }

    public function edit($id)
    {
        $record = DetalleAnalise::findOrFail($id);

        $this->selected_id = $id; 
		$this->analisis_id = $record-> analisis_id;
		$this->indicador = $record-> indicador;
		$this->resultado = $record-> resultado;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'analisis_id' => 'required',
		'indicador' => 'required',
		'resultado' => 'required',
        ]);

        if ($this->selected_id) {
			$record = DetalleAnalise::find($this->selected_id);
            $record->update([ 
			'analisis_id' => $this-> analisis_id,
			'indicador' => $this-> indicador,
			'resultado' => $this-> resultado
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'DetalleAnalise Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = DetalleAnalise::where('id', $id);
            $record->delete();
        }
    }
}
