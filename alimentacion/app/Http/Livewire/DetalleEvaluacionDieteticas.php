<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\DetalleEvaluacionDietetica;

class DetalleEvaluacionDieteticas extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $evaluacion_id, $tiempo, $hora, $lugar, $duracion;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.detalle-evaluacion-dieteticas.view', [
            'detalleEvaluacionDieteticas' => DetalleEvaluacionDietetica::latest()
						->orWhere('evaluacion_id', 'LIKE', $keyWord)
						->orWhere('tiempo', 'LIKE', $keyWord)
						->orWhere('hora', 'LIKE', $keyWord)
						->orWhere('lugar', 'LIKE', $keyWord)
						->orWhere('duracion', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->evaluacion_id = null;
		$this->tiempo = null;
		$this->hora = null;
		$this->lugar = null;
		$this->duracion = null;
    }

    public function store()
    {
        $this->validate([
		'evaluacion_id' => 'required',
		'tiempo' => 'required',
		'hora' => 'required',
		'lugar' => 'required',
		'duracion' => 'required',
        ]);

        DetalleEvaluacionDietetica::create([ 
			'evaluacion_id' => $this-> evaluacion_id,
			'tiempo' => $this-> tiempo,
			'hora' => $this-> hora,
			'lugar' => $this-> lugar,
			'duracion' => $this-> duracion
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'DetalleEvaluacionDietetica Successfully created.');
    }

    public function edit($id)
    {
        $record = DetalleEvaluacionDietetica::findOrFail($id);

        $this->selected_id = $id; 
		$this->evaluacion_id = $record-> evaluacion_id;
		$this->tiempo = $record-> tiempo;
		$this->hora = $record-> hora;
		$this->lugar = $record-> lugar;
		$this->duracion = $record-> duracion;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'evaluacion_id' => 'required',
		'tiempo' => 'required',
		'hora' => 'required',
		'lugar' => 'required',
		'duracion' => 'required',
        ]);

        if ($this->selected_id) {
			$record = DetalleEvaluacionDietetica::find($this->selected_id);
            $record->update([ 
			'evaluacion_id' => $this-> evaluacion_id,
			'tiempo' => $this-> tiempo,
			'hora' => $this-> hora,
			'lugar' => $this-> lugar,
			'duracion' => $this-> duracion
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'DetalleEvaluacionDietetica Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = DetalleEvaluacionDietetica::where('id', $id);
            $record->delete();
        }
    }
}
