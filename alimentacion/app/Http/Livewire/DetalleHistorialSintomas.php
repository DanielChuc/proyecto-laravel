<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\DetalleHistorialSintoma;

class DetalleHistorialSintomas extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $alimento_id, $historial_sintoma_id;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.detalle-historial-sintomas.view', [
            'detalleHistorialSintomas' => DetalleHistorialSintoma::latest()
						->orWhere('alimento_id', 'LIKE', $keyWord)
						->orWhere('historial_sintoma_id', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->alimento_id = null;
		$this->historial_sintoma_id = null;
    }

    public function store()
    {
        $this->validate([
		'alimento_id' => 'required',
		'historial_sintoma_id' => 'required',
        ]);

        DetalleHistorialSintoma::create([ 
			'alimento_id' => $this-> alimento_id,
			'historial_sintoma_id' => $this-> historial_sintoma_id
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'DetalleHistorialSintoma Successfully created.');
    }

    public function edit($id)
    {
        $record = DetalleHistorialSintoma::findOrFail($id);

        $this->selected_id = $id; 
		$this->alimento_id = $record-> alimento_id;
		$this->historial_sintoma_id = $record-> historial_sintoma_id;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'alimento_id' => 'required',
		'historial_sintoma_id' => 'required',
        ]);

        if ($this->selected_id) {
			$record = DetalleHistorialSintoma::find($this->selected_id);
            $record->update([ 
			'alimento_id' => $this-> alimento_id,
			'historial_sintoma_id' => $this-> historial_sintoma_id
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'DetalleHistorialSintoma Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = DetalleHistorialSintoma::where('id', $id);
            $record->delete();
        }
    }
}
