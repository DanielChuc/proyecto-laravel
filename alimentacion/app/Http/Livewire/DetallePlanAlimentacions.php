<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\DetallePlanAlimentacion;

class DetallePlanAlimentacions extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $plan_id, $tipocomida_id, $alimento_id, $cantidad, $unidad_medida, $nota, $dia_semana;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.detalle-plan-alimentacions.view', [
            'detallePlanAlimentacions' => DetallePlanAlimentacion::latest()
						->orWhere('plan_id', 'LIKE', $keyWord)
						->orWhere('tipocomida_id', 'LIKE', $keyWord)
						->orWhere('alimento_id', 'LIKE', $keyWord)
						->orWhere('cantidad', 'LIKE', $keyWord)
						->orWhere('unidad_medida', 'LIKE', $keyWord)
						->orWhere('nota', 'LIKE', $keyWord)
						->orWhere('dia_semana', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->plan_id = null;
		$this->tipocomida_id = null;
		$this->alimento_id = null;
		$this->cantidad = null;
		$this->unidad_medida = null;
		$this->nota = null;
		$this->dia_semana = null;
    }

    public function store()
    {
        $this->validate([
		'plan_id' => 'required',
		'tipocomida_id' => 'required',
		'alimento_id' => 'required',
		'cantidad' => 'required',
		'unidad_medida' => 'required',
		'nota' => 'required',
		'dia_semana' => 'required',
        ]);

        DetallePlanAlimentacion::create([ 
			'plan_id' => $this-> plan_id,
			'tipocomida_id' => $this-> tipocomida_id,
			'alimento_id' => $this-> alimento_id,
			'cantidad' => $this-> cantidad,
			'unidad_medida' => $this-> unidad_medida,
			'nota' => $this-> nota,
			'dia_semana' => $this-> dia_semana
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'DetallePlanAlimentacion Successfully created.');
    }

    public function edit($id)
    {
        $record = DetallePlanAlimentacion::findOrFail($id);

        $this->selected_id = $id; 
		$this->plan_id = $record-> plan_id;
		$this->tipocomida_id = $record-> tipocomida_id;
		$this->alimento_id = $record-> alimento_id;
		$this->cantidad = $record-> cantidad;
		$this->unidad_medida = $record-> unidad_medida;
		$this->nota = $record-> nota;
		$this->dia_semana = $record-> dia_semana;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'plan_id' => 'required',
		'tipocomida_id' => 'required',
		'alimento_id' => 'required',
		'cantidad' => 'required',
		'unidad_medida' => 'required',
		'nota' => 'required',
		'dia_semana' => 'required',
        ]);

        if ($this->selected_id) {
			$record = DetallePlanAlimentacion::find($this->selected_id);
            $record->update([ 
			'plan_id' => $this-> plan_id,
			'tipocomida_id' => $this-> tipocomida_id,
			'alimento_id' => $this-> alimento_id,
			'cantidad' => $this-> cantidad,
			'unidad_medida' => $this-> unidad_medida,
			'nota' => $this-> nota,
			'dia_semana' => $this-> dia_semana
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'DetallePlanAlimentacion Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = DetallePlanAlimentacion::where('id', $id);
            $record->delete();
        }
    }
}
