<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\EvaluacionDietetica;

class EvaluacionDieteticas extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $plan_id, $hora_apetito, $saciado, $quien_cocina;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.evaluacion-dieteticas.view', [
            'evaluacionDieteticas' => EvaluacionDietetica::latest()
						->orWhere('plan_id', 'LIKE', $keyWord)
						->orWhere('hora_apetito', 'LIKE', $keyWord)
						->orWhere('saciado', 'LIKE', $keyWord)
						->orWhere('quien_cocina', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->plan_id = null;
		$this->hora_apetito = null;
		$this->saciado = null;
		$this->quien_cocina = null;
    }

    public function store()
    {
        $this->validate([
		'plan_id' => 'required',
		'hora_apetito' => 'required',
		'saciado' => 'required',
		'quien_cocina' => 'required',
        ]);

        EvaluacionDietetica::create([ 
			'plan_id' => $this-> plan_id,
			'hora_apetito' => $this-> hora_apetito,
			'saciado' => $this-> saciado,
			'quien_cocina' => $this-> quien_cocina
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'EvaluacionDietetica Successfully created.');
    }

    public function edit($id)
    {
        $record = EvaluacionDietetica::findOrFail($id);

        $this->selected_id = $id; 
		$this->plan_id = $record-> plan_id;
		$this->hora_apetito = $record-> hora_apetito;
		$this->saciado = $record-> saciado;
		$this->quien_cocina = $record-> quien_cocina;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'plan_id' => 'required',
		'hora_apetito' => 'required',
		'saciado' => 'required',
		'quien_cocina' => 'required',
        ]);

        if ($this->selected_id) {
			$record = EvaluacionDietetica::find($this->selected_id);
            $record->update([ 
			'plan_id' => $this-> plan_id,
			'hora_apetito' => $this-> hora_apetito,
			'saciado' => $this-> saciado,
			'quien_cocina' => $this-> quien_cocina
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'EvaluacionDietetica Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = EvaluacionDietetica::where('id', $id);
            $record->delete();
        }
    }
}
