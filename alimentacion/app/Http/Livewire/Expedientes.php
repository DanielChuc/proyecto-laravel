<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Expediente;

class Expedientes extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $nombre, $apellidos, $ocupacion, $telefono, $email, $genero, $quienvive, $direccion, $cirugias_previas, $sabores, $user_id;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.expedientes.view', [
            'expedientes' => Expediente::latest()
						->orWhere('nombre', 'LIKE', $keyWord)
						->orWhere('apellidos', 'LIKE', $keyWord)
						->orWhere('ocupacion', 'LIKE', $keyWord)
						->orWhere('telefono', 'LIKE', $keyWord)
						->orWhere('email', 'LIKE', $keyWord)
						->orWhere('genero', 'LIKE', $keyWord)
						->orWhere('quienvive', 'LIKE', $keyWord)
						->orWhere('direccion', 'LIKE', $keyWord)
						->orWhere('cirugias_previas', 'LIKE', $keyWord)
						->orWhere('sabores', 'LIKE', $keyWord)
						->orWhere('user_id', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->nombre = null;
		$this->apellidos = null;
		$this->ocupacion = null;
		$this->telefono = null;
		$this->email = null;
		$this->genero = null;
		$this->quienvive = null;
		$this->direccion = null;
		$this->cirugias_previas = null;
		$this->sabores = null;
		$this->user_id = null;
    }

    public function store()
    {
        $this->validate([
		'nombre' => 'required',
		'apellidos' => 'required',
		'ocupacion' => 'required',
		'telefono' => 'required',
		'email' => 'required',
		'genero' => 'required',
		'quienvive' => 'required',
		'direccion' => 'required',
		'cirugias_previas' => 'required',
		'sabores' => 'required',
		'user_id' => 'required',
        ]);

        Expediente::create([ 
			'nombre' => $this-> nombre,
			'apellidos' => $this-> apellidos,
			'ocupacion' => $this-> ocupacion,
			'telefono' => $this-> telefono,
			'email' => $this-> email,
			'genero' => $this-> genero,
			'quienvive' => $this-> quienvive,
			'direccion' => $this-> direccion,
			'cirugias_previas' => $this-> cirugias_previas,
			'sabores' => $this-> sabores,
			'user_id' => $this-> user_id
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'Expediente Successfully created.');
    }

    public function edit($id)
    {
        $record = Expediente::findOrFail($id);

        $this->selected_id = $id; 
		$this->nombre = $record-> nombre;
		$this->apellidos = $record-> apellidos;
		$this->ocupacion = $record-> ocupacion;
		$this->telefono = $record-> telefono;
		$this->email = $record-> email;
		$this->genero = $record-> genero;
		$this->quienvive = $record-> quienvive;
		$this->direccion = $record-> direccion;
		$this->cirugias_previas = $record-> cirugias_previas;
		$this->sabores = $record-> sabores;
		$this->user_id = $record-> user_id;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'nombre' => 'required',
		'apellidos' => 'required',
		'ocupacion' => 'required',
		'telefono' => 'required',
		'email' => 'required',
		'genero' => 'required',
		'quienvive' => 'required',
		'direccion' => 'required',
		'cirugias_previas' => 'required',
		'sabores' => 'required',
		'user_id' => 'required',
        ]);

        if ($this->selected_id) {
			$record = Expediente::find($this->selected_id);
            $record->update([ 
			'nombre' => $this-> nombre,
			'apellidos' => $this-> apellidos,
			'ocupacion' => $this-> ocupacion,
			'telefono' => $this-> telefono,
			'email' => $this-> email,
			'genero' => $this-> genero,
			'quienvive' => $this-> quienvive,
			'direccion' => $this-> direccion,
			'cirugias_previas' => $this-> cirugias_previas,
			'sabores' => $this-> sabores,
			'user_id' => $this-> user_id
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'Expediente Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Expediente::where('id', $id);
            $record->delete();
        }
    }
}
