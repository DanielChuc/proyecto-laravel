<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\FrecuenciaAlimentaria;

class FrecuenciaAlimentarias extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $plan_id, $alimento_id, $dias_semana;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.frecuencia-alimentarias.view', [
            'frecuenciaAlimentarias' => FrecuenciaAlimentaria::latest()
						->orWhere('plan_id', 'LIKE', $keyWord)
						->orWhere('alimento_id', 'LIKE', $keyWord)
						->orWhere('dias_semana', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->plan_id = null;
		$this->alimento_id = null;
		$this->dias_semana = null;
    }

    public function store()
    {
        $this->validate([
		'plan_id' => 'required',
		'alimento_id' => 'required',
		'dias_semana' => 'required',
        ]);

        FrecuenciaAlimentaria::create([ 
			'plan_id' => $this-> plan_id,
			'alimento_id' => $this-> alimento_id,
			'dias_semana' => $this-> dias_semana
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'FrecuenciaAlimentaria Successfully created.');
    }

    public function edit($id)
    {
        $record = FrecuenciaAlimentaria::findOrFail($id);

        $this->selected_id = $id; 
		$this->plan_id = $record-> plan_id;
		$this->alimento_id = $record-> alimento_id;
		$this->dias_semana = $record-> dias_semana;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'plan_id' => 'required',
		'alimento_id' => 'required',
		'dias_semana' => 'required',
        ]);

        if ($this->selected_id) {
			$record = FrecuenciaAlimentaria::find($this->selected_id);
            $record->update([ 
			'plan_id' => $this-> plan_id,
			'alimento_id' => $this-> alimento_id,
			'dias_semana' => $this-> dias_semana
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'FrecuenciaAlimentaria Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = FrecuenciaAlimentaria::where('id', $id);
            $record->delete();
        }
    }
}
