<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Habito;

class Habitos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $expediente_id, $cant_cigarro, $act_fisica, $consumo_agua, $azucar, $sal;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.habitos.view', [
            'habitos' => Habito::latest()
						->orWhere('expediente_id', 'LIKE', $keyWord)
						->orWhere('cant_cigarro', 'LIKE', $keyWord)
						->orWhere('act_fisica', 'LIKE', $keyWord)
						->orWhere('consumo_agua', 'LIKE', $keyWord)
						->orWhere('azucar', 'LIKE', $keyWord)
						->orWhere('sal', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->expediente_id = null;
		$this->cant_cigarro = null;
		$this->act_fisica = null;
		$this->consumo_agua = null;
		$this->azucar = null;
		$this->sal = null;
    }

    public function store()
    {
        $this->validate([
		'expediente_id' => 'required',
		'cant_cigarro' => 'required',
		'act_fisica' => 'required',
		'consumo_agua' => 'required',
		'azucar' => 'required',
		'sal' => 'required',
        ]);

        Habito::create([ 
			'expediente_id' => $this-> expediente_id,
			'cant_cigarro' => $this-> cant_cigarro,
			'act_fisica' => $this-> act_fisica,
			'consumo_agua' => $this-> consumo_agua,
			'azucar' => $this-> azucar,
			'sal' => $this-> sal
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'Habito Successfully created.');
    }

    public function edit($id)
    {
        $record = Habito::findOrFail($id);

        $this->selected_id = $id; 
		$this->expediente_id = $record-> expediente_id;
		$this->cant_cigarro = $record-> cant_cigarro;
		$this->act_fisica = $record-> act_fisica;
		$this->consumo_agua = $record-> consumo_agua;
		$this->azucar = $record-> azucar;
		$this->sal = $record-> sal;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'expediente_id' => 'required',
		'cant_cigarro' => 'required',
		'act_fisica' => 'required',
		'consumo_agua' => 'required',
		'azucar' => 'required',
		'sal' => 'required',
        ]);

        if ($this->selected_id) {
			$record = Habito::find($this->selected_id);
            $record->update([ 
			'expediente_id' => $this-> expediente_id,
			'cant_cigarro' => $this-> cant_cigarro,
			'act_fisica' => $this-> act_fisica,
			'consumo_agua' => $this-> consumo_agua,
			'azucar' => $this-> azucar,
			'sal' => $this-> sal
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'Habito Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Habito::where('id', $id);
            $record->delete();
        }
    }
}
