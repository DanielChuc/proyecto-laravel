<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\HistorialMedicamento;

class HistorialMedicamentos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $expediente_id, $nombre, $frecuencia_horaria, $dosis;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.historial-medicamentos.view', [
            'historialMedicamentos' => HistorialMedicamento::latest()
						->orWhere('expediente_id', 'LIKE', $keyWord)
						->orWhere('nombre', 'LIKE', $keyWord)
						->orWhere('frecuencia_horaria', 'LIKE', $keyWord)
						->orWhere('dosis', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->expediente_id = null;
		$this->nombre = null;
		$this->frecuencia_horaria = null;
		$this->dosis = null;
    }

    public function store()
    {
        $this->validate([
		'expediente_id' => 'required',
		'nombre' => 'required',
		'frecuencia_horaria' => 'required',
		'dosis' => 'required',
        ]);

        HistorialMedicamento::create([ 
			'expediente_id' => $this-> expediente_id,
			'nombre' => $this-> nombre,
			'frecuencia_horaria' => $this-> frecuencia_horaria,
			'dosis' => $this-> dosis
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'HistorialMedicamento Successfully created.');
    }

    public function edit($id)
    {
        $record = HistorialMedicamento::findOrFail($id);

        $this->selected_id = $id; 
		$this->expediente_id = $record-> expediente_id;
		$this->nombre = $record-> nombre;
		$this->frecuencia_horaria = $record-> frecuencia_horaria;
		$this->dosis = $record-> dosis;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'expediente_id' => 'required',
		'nombre' => 'required',
		'frecuencia_horaria' => 'required',
		'dosis' => 'required',
        ]);

        if ($this->selected_id) {
			$record = HistorialMedicamento::find($this->selected_id);
            $record->update([ 
			'expediente_id' => $this-> expediente_id,
			'nombre' => $this-> nombre,
			'frecuencia_horaria' => $this-> frecuencia_horaria,
			'dosis' => $this-> dosis
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'HistorialMedicamento Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = HistorialMedicamento::where('id', $id);
            $record->delete();
        }
    }
}
