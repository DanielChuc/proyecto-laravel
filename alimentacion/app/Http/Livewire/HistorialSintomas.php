<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\HistorialSintoma;

class HistorialSintomas extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $expediente_id, $nombre, $antiguedad, $hora;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.historial-sintomas.view', [
            'historialSintomas' => HistorialSintoma::latest()
						->orWhere('expediente_id', 'LIKE', $keyWord)
						->orWhere('nombre', 'LIKE', $keyWord)
						->orWhere('antiguedad', 'LIKE', $keyWord)
						->orWhere('hora', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->expediente_id = null;
		$this->nombre = null;
		$this->antiguedad = null;
		$this->hora = null;
    }

    public function store()
    {
        $this->validate([
		'expediente_id' => 'required',
		'nombre' => 'required',
		'antiguedad' => 'required',
		'hora' => 'required',
        ]);

        HistorialSintoma::create([ 
			'expediente_id' => $this-> expediente_id,
			'nombre' => $this-> nombre,
			'antiguedad' => $this-> antiguedad,
			'hora' => $this-> hora
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'HistorialSintoma Successfully created.');
    }

    public function edit($id)
    {
        $record = HistorialSintoma::findOrFail($id);

        $this->selected_id = $id; 
		$this->expediente_id = $record-> expediente_id;
		$this->nombre = $record-> nombre;
		$this->antiguedad = $record-> antiguedad;
		$this->hora = $record-> hora;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'expediente_id' => 'required',
		'nombre' => 'required',
		'antiguedad' => 'required',
		'hora' => 'required',
        ]);

        if ($this->selected_id) {
			$record = HistorialSintoma::find($this->selected_id);
            $record->update([ 
			'expediente_id' => $this-> expediente_id,
			'nombre' => $this-> nombre,
			'antiguedad' => $this-> antiguedad,
			'hora' => $this-> hora
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'HistorialSintoma Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = HistorialSintoma::where('id', $id);
            $record->delete();
        }
    }
}
