<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Intolerancia;

class Intolerancias extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $plan_id, $alimento_id;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.intolerancias.view', [
            'intolerancias' => Intolerancia::latest()
						->orWhere('plan_id', 'LIKE', $keyWord)
						->orWhere('alimento_id', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->plan_id = null;
		$this->alimento_id = null;
    }

    public function store()
    {
        $this->validate([
		'plan_id' => 'required',
		'alimento_id' => 'required',
        ]);

        Intolerancia::create([ 
			'plan_id' => $this-> plan_id,
			'alimento_id' => $this-> alimento_id
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'Intolerancia Successfully created.');
    }

    public function edit($id)
    {
        $record = Intolerancia::findOrFail($id);

        $this->selected_id = $id; 
		$this->plan_id = $record-> plan_id;
		$this->alimento_id = $record-> alimento_id;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'plan_id' => 'required',
		'alimento_id' => 'required',
        ]);

        if ($this->selected_id) {
			$record = Intolerancia::find($this->selected_id);
            $record->update([ 
			'plan_id' => $this-> plan_id,
			'alimento_id' => $this-> alimento_id
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'Intolerancia Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Intolerancia::where('id', $id);
            $record->delete();
        }
    }
}
