<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\PlanAlimentacion;

class PlanAlimentacions extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $expediente_id, $fecha_inicio, $fecha_fin, $objetivo, $total_diario;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.plan-alimentacions.view', [
            'planAlimentacions' => PlanAlimentacion::latest()
						->orWhere('expediente_id', 'LIKE', $keyWord)
						->orWhere('fecha_inicio', 'LIKE', $keyWord)
						->orWhere('fecha_fin', 'LIKE', $keyWord)
						->orWhere('objetivo', 'LIKE', $keyWord)
						->orWhere('total_diario', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->expediente_id = null;
		$this->fecha_inicio = null;
		$this->fecha_fin = null;
		$this->objetivo = null;
		$this->total_diario = null;
    }

    public function store()
    {
        $this->validate([
		'expediente_id' => 'required',
		'fecha_inicio' => 'required',
		'fecha_fin' => 'required',
		'objetivo' => 'required',
		'total_diario' => 'required',
        ]);

        PlanAlimentacion::create([ 
			'expediente_id' => $this-> expediente_id,
			'fecha_inicio' => $this-> fecha_inicio,
			'fecha_fin' => $this-> fecha_fin,
			'objetivo' => $this-> objetivo,
			'total_diario' => $this-> total_diario
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'PlanAlimentacion Successfully created.');
    }

    public function edit($id)
    {
        $record = PlanAlimentacion::findOrFail($id);

        $this->selected_id = $id; 
		$this->expediente_id = $record-> expediente_id;
		$this->fecha_inicio = $record-> fecha_inicio;
		$this->fecha_fin = $record-> fecha_fin;
		$this->objetivo = $record-> objetivo;
		$this->total_diario = $record-> total_diario;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'expediente_id' => 'required',
		'fecha_inicio' => 'required',
		'fecha_fin' => 'required',
		'objetivo' => 'required',
		'total_diario' => 'required',
        ]);

        if ($this->selected_id) {
			$record = PlanAlimentacion::find($this->selected_id);
            $record->update([ 
			'expediente_id' => $this-> expediente_id,
			'fecha_inicio' => $this-> fecha_inicio,
			'fecha_fin' => $this-> fecha_fin,
			'objetivo' => $this-> objetivo,
			'total_diario' => $this-> total_diario
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'PlanAlimentacion Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = PlanAlimentacion::where('id', $id);
            $record->delete();
        }
    }
}
