<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Socioeconomico;

class Socioeconomicos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $expediente_id, $presupuesto, $utensilios;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.socioeconomicos.view', [
            'socioeconomicos' => Socioeconomico::latest()
						->orWhere('expediente_id', 'LIKE', $keyWord)
						->orWhere('presupuesto', 'LIKE', $keyWord)
						->orWhere('utensilios', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->expediente_id = null;
		$this->presupuesto = null;
		$this->utensilios = null;
    }

    public function store()
    {
        $this->validate([
		'expediente_id' => 'required',
		'presupuesto' => 'required',
		'utensilios' => 'required',
        ]);

        Socioeconomico::create([ 
			'expediente_id' => $this-> expediente_id,
			'presupuesto' => $this-> presupuesto,
			'utensilios' => $this-> utensilios
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'Socioeconomico Successfully created.');
    }

    public function edit($id)
    {
        $record = Socioeconomico::findOrFail($id);

        $this->selected_id = $id; 
		$this->expediente_id = $record-> expediente_id;
		$this->presupuesto = $record-> presupuesto;
		$this->utensilios = $record-> utensilios;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'expediente_id' => 'required',
		'presupuesto' => 'required',
		'utensilios' => 'required',
        ]);

        if ($this->selected_id) {
			$record = Socioeconomico::find($this->selected_id);
            $record->update([ 
			'expediente_id' => $this-> expediente_id,
			'presupuesto' => $this-> presupuesto,
			'utensilios' => $this-> utensilios
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'Socioeconomico Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Socioeconomico::where('id', $id);
            $record->delete();
        }
    }
}
