<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\TipoAlimento;

class TipoAlimentos extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $tipoalimento;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.tipo-alimentos.view', [
            'tipoAlimentos' => TipoAlimento::latest()
						->orWhere('tipoalimento', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->tipoalimento = null;
    }

    public function store()
    {
        $this->validate([
		'tipoalimento' => 'required',
        ]);

        TipoAlimento::create([ 
			'tipoalimento' => $this-> tipoalimento
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'TipoAlimento Successfully created.');
    }

    public function edit($id)
    {
        $record = TipoAlimento::findOrFail($id);

        $this->selected_id = $id; 
		$this->tipoalimento = $record-> tipoalimento;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'tipoalimento' => 'required',
        ]);

        if ($this->selected_id) {
			$record = TipoAlimento::find($this->selected_id);
            $record->update([ 
			'tipoalimento' => $this-> tipoalimento
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'TipoAlimento Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = TipoAlimento::where('id', $id);
            $record->delete();
        }
    }
}
