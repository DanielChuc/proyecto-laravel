<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\TipoComida;

class TipoComidas extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $tipocomida;
    public $updateMode = false;

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.tipo-comidas.view', [
            'tipoComidas' => TipoComida::latest()
						->orWhere('tipocomida', 'LIKE', $keyWord)
						->paginate(10),
        ]);
    }
	
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }
	
    private function resetInput()
    {		
		$this->tipocomida = null;
    }

    public function store()
    {
        $this->validate([
		'tipocomida' => 'required',
        ]);

        TipoComida::create([ 
			'tipocomida' => $this-> tipocomida
        ]);
        
        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'TipoComida Successfully created.');
    }

    public function edit($id)
    {
        $record = TipoComida::findOrFail($id);

        $this->selected_id = $id; 
		$this->tipocomida = $record-> tipocomida;
		
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'tipocomida' => 'required',
        ]);

        if ($this->selected_id) {
			$record = TipoComida::find($this->selected_id);
            $record->update([ 
			'tipocomida' => $this-> tipocomida
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'TipoComida Successfully updated.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = TipoComida::where('id', $id);
            $record->delete();
        }
    }
}
