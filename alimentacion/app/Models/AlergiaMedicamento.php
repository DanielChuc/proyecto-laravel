<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlergiaMedicamento extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'alergia_medicamentos';

    protected $fillable = ['expediente_id','nombre'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function expediente()
    {
        return $this->hasOne('App\Models\Expediente', 'id', 'expediente_id');
    }
    
}
