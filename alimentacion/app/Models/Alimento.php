<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alimento extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'alimentos';

    protected $fillable = ['nombre','tipoalimento_id','descripcion','ingredientes','preparacion','calorias','urlvideo','urlfotos'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function alergias()
    {
        return $this->hasMany('App\Models\Alergia', 'alimento_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detalleHistorialSintomas()
    {
        return $this->hasMany('App\Models\DetalleHistorialSintoma', 'alimento_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detallePlanAlimentacions()
    {
        return $this->hasMany('App\Models\DetallePlanAlimentacion', 'alimento_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function frecuenciaAlimentarias()
    {
        return $this->hasMany('App\Models\FrecuenciaAlimentaria', 'alimento_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function intolerancias()
    {
        return $this->hasMany('App\Models\Intolerancia', 'alimento_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function tipoAlimento()
    {
        return $this->hasOne('App\Models\TipoAlimento', 'id', 'tipoalimento_id');
    }
    
}
