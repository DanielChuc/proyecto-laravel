<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnalisisBioquimico extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'analisis_bioquimicos';

    protected $fillable = ['plan_id','fecha'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detalleAnalises()
    {
        return $this->hasMany('App\Models\DetalleAnalise', 'analisis_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function planAlimentacion()
    {
        return $this->hasOne('App\Models\PlanAlimentacion', 'id', 'plan_id');
    }
    
}
