<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Antecedente extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'antecedentes';

    protected $fillable = ['tipo','nombre','detalle','expediente_id'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function antecedentepadecimientos()
    {
        return $this->hasMany('App\Models\Antecedentepadecimiento', 'antecedente_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function expediente()
    {
        return $this->hasOne('App\Models\Expediente', 'id', 'expediente_id');
    }
    
}
