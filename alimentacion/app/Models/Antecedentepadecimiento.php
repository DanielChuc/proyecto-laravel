<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Antecedentepadecimiento extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'antecedentepadecimientos';

    protected $fillable = ['antecedente_id','padecimiento_id'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function antecedente()
    {
        return $this->hasOne('App\Models\Antecedente', 'id', 'antecedente_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function padecimiento()
    {
        return $this->hasOne('App\Models\Padecimiento', 'id', 'padecimiento_id');
    }
    
}
