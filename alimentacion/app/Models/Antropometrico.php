<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Antropometrico extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'antropometricos';

    protected $fillable = ['plan_id','indicador_id','valor'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function indicadorAntropometrico()
    {
        return $this->hasOne('App\Models\IndicadorAntropometrico', 'id', 'indicador_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function planAlimentacion()
    {
        return $this->hasOne('App\Models\PlanAlimentacion', 'id', 'plan_id');
    }
    
}
