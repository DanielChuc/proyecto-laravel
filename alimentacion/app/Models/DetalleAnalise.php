<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleAnalise extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'detalle_analises';

    protected $fillable = ['analisis_id','indicador','resultado'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function analisisBioquimico()
    {
        return $this->hasOne('App\Models\AnalisisBioquimico', 'id', 'analisis_id');
    }
    
}
