<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleEvaluacionDietetica extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'detalle_evaluacion_dieteticas';

    protected $fillable = ['evaluacion_id','tiempo','hora','lugar','duracion'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function evaluacionDietetica()
    {
        return $this->hasOne('App\Models\EvaluacionDietetica', 'id', 'evaluacion_id');
    }
    
}
