<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleHistorialSintoma extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'detalle_historial_sintomas';

    protected $fillable = ['alimento_id','historial_sintoma_id'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function alimento()
    {
        return $this->hasOne('App\Models\Alimento', 'id', 'alimento_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function historialSintoma()
    {
        return $this->hasOne('App\Models\HistorialSintoma', 'id', 'historial_sintoma_id');
    }
    
}
