<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetallePlanAlimentacion extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'detalle_plan_alimentacions';

    protected $fillable = ['plan_id','tipocomida_id','alimento_id','cantidad','unidad_medida','nota','dia_semana'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function alimento()
    {
        return $this->hasOne('App\Models\Alimento', 'id', 'alimento_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function planAlimentacion()
    {
        return $this->hasOne('App\Models\PlanAlimentacion', 'id', 'plan_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function tipoComida()
    {
        return $this->hasOne('App\Models\TipoComida', 'id', 'tipocomida_id');
    }
    
}
