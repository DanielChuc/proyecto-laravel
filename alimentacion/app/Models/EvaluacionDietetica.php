<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EvaluacionDietetica extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'evaluacion_dieteticas';

    protected $fillable = ['plan_id','hora_apetito','saciado','quien_cocina'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detalleEvaluacionDieteticas()
    {
        return $this->hasMany('App\Models\DetalleEvaluacionDietetica', 'evaluacion_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function planAlimentacion()
    {
        return $this->hasOne('App\Models\PlanAlimentacion', 'id', 'plan_id');
    }
    
}
