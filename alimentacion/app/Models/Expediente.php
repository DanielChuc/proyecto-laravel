<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expediente extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'expedientes';

    protected $fillable = ['nombre','apellidos','ocupacion','telefono','email','genero','quienvive','direccion','cirugias_previas','sabores','user_id'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function alergiaMedicamentos()
    {
        return $this->hasMany('App\Models\AlergiaMedicamento', 'expediente_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function antecedentes()
    {
        return $this->hasMany('App\Models\Antecedente', 'expediente_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function habitos()
    {
        return $this->hasMany('App\Models\Habito', 'expediente_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function historialMedicamentos()
    {
        return $this->hasMany('App\Models\HistorialMedicamento', 'expediente_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function historialSintomas()
    {
        return $this->hasMany('App\Models\HistorialSintoma', 'expediente_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function planAlimentacions()
    {
        return $this->hasMany('App\Models\PlanAlimentacion', 'expediente_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function socioeconomicos()
    {
        return $this->hasMany('App\Models\Socioeconomico', 'expediente_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    
}
