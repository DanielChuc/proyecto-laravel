<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Habito extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'habitos';

    protected $fillable = ['expediente_id','cant_cigarro','act_fisica','consumo_agua','azucar','sal'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function expediente()
    {
        return $this->hasOne('App\Models\Expediente', 'id', 'expediente_id');
    }
    
}
