<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistorialMedicamento extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'historial_medicamentos';

    protected $fillable = ['expediente_id','nombre','frecuencia_horaria','dosis'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function expediente()
    {
        return $this->hasOne('App\Models\Expediente', 'id', 'expediente_id');
    }
    
}
