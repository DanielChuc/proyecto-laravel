<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistorialSintoma extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'historial_sintomas';

    protected $fillable = ['expediente_id','nombre','antiguedad','hora'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detalleHistorialSintomas()
    {
        return $this->hasMany('App\Models\DetalleHistorialSintoma', 'historial_sintoma_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function expediente()
    {
        return $this->hasOne('App\Models\Expediente', 'id', 'expediente_id');
    }
    
}
