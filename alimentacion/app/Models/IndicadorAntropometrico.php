<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndicadorAntropometrico extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'indicador_antropometricos';

    protected $fillable = ['nombre'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function antropometricos()
    {
        return $this->hasMany('App\Models\Antropometrico', 'indicador_id', 'id');
    }
    
}
