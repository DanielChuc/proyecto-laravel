<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Intolerancia extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'intolerancias';

    protected $fillable = ['plan_id','alimento_id'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function alimento()
    {
        return $this->hasOne('App\Models\Alimento', 'id', 'alimento_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function planAlimentacion()
    {
        return $this->hasOne('App\Models\PlanAlimentacion', 'id', 'plan_id');
    }
    
}
