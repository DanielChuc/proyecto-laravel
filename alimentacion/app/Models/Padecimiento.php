<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Padecimiento extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'padecimientos';

    protected $fillable = ['nombre','descripcion'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function antecedentepadecimientos()
    {
        return $this->hasMany('App\Models\Antecedentepadecimiento', 'padecimiento_id', 'id');
    }
    
}
