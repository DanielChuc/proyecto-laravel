<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanAlimentacion extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'plan_alimentacions';

    protected $fillable = ['expediente_id','fecha_inicio','fecha_fin','objetivo','total_diario'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function alergias()
    {
        return $this->hasMany('App\Models\Alergia', 'plan_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function analisisBioquimicos()
    {
        return $this->hasMany('App\Models\AnalisisBioquimico', 'plan_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function antropometricos()
    {
        return $this->hasMany('App\Models\Antropometrico', 'plan_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detallePlanAlimentacions()
    {
        return $this->hasMany('App\Models\DetallePlanAlimentacion', 'plan_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evaluacionDieteticas()
    {
        return $this->hasMany('App\Models\EvaluacionDietetica', 'plan_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function expediente()
    {
        return $this->hasOne('App\Models\Expediente', 'id', 'expediente_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function frecuenciaAlimentarias()
    {
        return $this->hasMany('App\Models\FrecuenciaAlimentaria', 'plan_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function intolerancias()
    {
        return $this->hasMany('App\Models\Intolerancia', 'plan_id', 'id');
    }
    
}
