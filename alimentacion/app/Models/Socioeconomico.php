<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Socioeconomico extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'socioeconomicos';

    protected $fillable = ['expediente_id','presupuesto','utensilios'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function expediente()
    {
        return $this->hasOne('App\Models\Expediente', 'id', 'expediente_id');
    }
    
}
