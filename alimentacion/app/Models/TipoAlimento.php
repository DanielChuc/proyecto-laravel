<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoAlimento extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'tipo_alimentos';

    protected $fillable = ['tipoalimento'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function alimentos()
    {
        return $this->hasMany('App\Models\Alimento', 'tipoalimento_id', 'id');
    }
    
}
