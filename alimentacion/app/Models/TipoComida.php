<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoComida extends Model
{
	use HasFactory;
	
    public $timestamps = true;

    protected $table = 'tipo_comidas';

    protected $fillable = ['tipocomida'];
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detallePlanAlimentacions()
    {
        return $this->hasMany('App\Models\DetallePlanAlimentacion', 'tipocomida_id', 'id');
    }
    
}
