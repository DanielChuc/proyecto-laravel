<?php

namespace Database\Factories;

use App\Models\Alergia;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AlergiaFactory extends Factory
{
    protected $model = Alergia::class;

    public function definition()
    {
        return [
			'plan_id' => Str::random(10),
			'alimento_id' => Str::random(10),
        ];
    }
}
