<?php

namespace Database\Factories;

use App\Models\AlergiaMedicamento;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AlergiaMedicamentoFactory extends Factory
{
    protected $model = AlergiaMedicamento::class;

    public function definition()
    {
        return [
			'expediente_id' => Str::random(10),
			'nombre' => Str::random(10),
        ];
    }
}
