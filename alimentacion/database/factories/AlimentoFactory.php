<?php

namespace Database\Factories;

use App\Models\Alimento;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AlimentoFactory extends Factory
{
    protected $model = Alimento::class;

    public function definition()
    {
        return [
			'nombre' => $this->faker->name,
			'tipoalimento_id' => Str::random(10),
			'descripcion' => $this->faker->description,
			'ingredientes' => $this->faker->name,
			'preparacion' => $this->faker->name,
			'calorias' => $this->faker->name,
			'urlvideo' => $this->faker->name,
			'urlfotos' => $this->faker->name,
        ];
    }
}
