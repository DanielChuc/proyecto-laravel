<?php

namespace Database\Factories;

use App\Models\AnalisisBioquimico;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AnalisisBioquimicoFactory extends Factory
{
    protected $model = AnalisisBioquimico::class;

    public function definition()
    {
        return [
			'plan_id' => $this->faker->name,
			'fecha' => $this->faker->name,
        ];
    }
}
