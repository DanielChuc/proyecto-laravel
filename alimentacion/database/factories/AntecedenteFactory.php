<?php

namespace Database\Factories;

use App\Models\Antecedente;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AntecedenteFactory extends Factory
{
    protected $model = Antecedente::class;

    public function definition()
    {
        return [
			'tipo' => $this->faker->word,
			'nombre' => $this->faker->name,
			'detalle' => $this->faker->sentence,
			'expediente_id' => \App\Models\Expediente::inRandomOrder()->first()->id,
        ];
    }
}
