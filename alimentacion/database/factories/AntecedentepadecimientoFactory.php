<?php

namespace Database\Factories;

use App\Models\Antecedentepadecimiento;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AntecedentepadecimientoFactory extends Factory
{
    protected $model = Antecedentepadecimiento::class;

    public function definition()
    {
        return [
			'antecedente_id' => $this->faker->name,
			'padecimiento_id' => $this->faker->name,
        ];
    }
}
