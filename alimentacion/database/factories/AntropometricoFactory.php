<?php

namespace Database\Factories;

use App\Models\Antropometrico;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AntropometricoFactory extends Factory
{
    protected $model = Antropometrico::class;

    public function definition()
    {
        return [
			'plan_id' => $this->faker->name,
			'indicador_id' => $this->faker->name,
			'valor' => $this->faker->name,
        ];
    }
}
