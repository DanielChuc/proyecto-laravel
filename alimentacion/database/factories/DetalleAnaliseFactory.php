<?php

namespace Database\Factories;

use App\Models\DetalleAnalise;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DetalleAnaliseFactory extends Factory
{
    protected $model = DetalleAnalise::class;

    public function definition()
    {
        return [
			'analisis_id' => $this->faker->name,
			'indicador' => $this->faker->name,
			'resultado' => $this->faker->name,
        ];
    }
}
