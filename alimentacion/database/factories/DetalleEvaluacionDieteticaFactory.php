<?php

namespace Database\Factories;

use App\Models\DetalleEvaluacionDietetica;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DetalleEvaluacionDieteticaFactory extends Factory
{
    protected $model = DetalleEvaluacionDietetica::class;

    public function definition()
    {
        return [
			'evaluacion_id' => $this->faker->name,
			'tiempo' => $this->faker->name,
			'hora' => $this->faker->name,
			'lugar' => $this->faker->name,
			'duracion' => $this->faker->name,
        ];
    }
}
