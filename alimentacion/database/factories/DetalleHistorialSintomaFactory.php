<?php

namespace Database\Factories;

use App\Models\DetalleHistorialSintoma;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DetalleHistorialSintomaFactory extends Factory
{
    protected $model = DetalleHistorialSintoma::class;

    public function definition()
    {
        return [
			'alimento_id' => $this->faker->name,
			'historial_sintoma_id' => $this->faker->name,
        ];
    }
}
