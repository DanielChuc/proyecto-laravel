<?php

namespace Database\Factories;

use App\Models\DetallePlanAlimentacion;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DetallePlanAlimentacionFactory extends Factory
{
    protected $model = DetallePlanAlimentacion::class;

    public function definition()
    {
        return [
			'plan_id' => $this->faker->name,
			'tipocomida_id' => $this->faker->name,
			'alimento_id' => $this->faker->name,
			'cantidad' => $this->faker->name,
			'unidad_medida' => $this->faker->name,
			'nota' => $this->faker->name,
			'dia_semana' => $this->faker->name,
        ];
    }
}
