<?php

namespace Database\Factories;

use App\Models\EvaluacionDietetica;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EvaluacionDieteticaFactory extends Factory
{
    protected $model = EvaluacionDietetica::class;

    public function definition()
    {
        return [
			'plan_id' => $this->faker->name,
			'hora_apetito' => $this->faker->name,
			'saciado' => $this->faker->name,
			'quien_cocina' => $this->faker->name,
        ];
    }
}
