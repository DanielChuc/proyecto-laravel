<?php

namespace Database\Factories;

use App\Models\Expediente;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ExpedienteFactory extends Factory
{
    protected $model = Expediente::class;

    public function definition()
    {
        return [
			'nombre' => $this->faker->name,
			'apellidos' => $this->faker->lastName,
			'ocupacion' => $this->faker->name,
			'telefono' => $this->faker->e164PhoneNumber,
			'email' => $this->faker->email,
			'genero' => $this->faker->randomElement(['Hombre','Mujer']),
			'quienvive' => $this->faker->name,
			'direccion' => $this->faker->address,
			'cirugias_previas' =>  $this->faker->randomElement(['Si','No']),
			'sabores' => $this->faker->sentence,
			'user_id' =>  \App\Models\User::inRandomOrder()->first()->id,
        ];
    }
}
