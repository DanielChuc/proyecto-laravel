<?php

namespace Database\Factories;

use App\Models\FrecuenciaAlimentaria;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class FrecuenciaAlimentariaFactory extends Factory
{
    protected $model = FrecuenciaAlimentaria::class;

    public function definition()
    {
        return [
			'plan_id' => $this->faker->name,
			'alimento_id' => $this->faker->name,
			'dias_semana' => $this->faker->name,
        ];
    }
}
