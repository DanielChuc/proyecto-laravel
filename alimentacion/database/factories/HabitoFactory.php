<?php

namespace Database\Factories;

use App\Models\Habito;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class HabitoFactory extends Factory
{
    protected $model = Habito::class;

    public function definition()
    {
        return [
			'expediente_id' => $this->faker->name,
			'cant_cigarro' => $this->faker->name,
			'act_fisica' => $this->faker->name,
			'consumo_agua' => $this->faker->name,
			'azucar' => $this->faker->name,
			'sal' => $this->faker->name,
        ];
    }
}
