<?php

namespace Database\Factories;

use App\Models\HistorialMedicamento;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class HistorialMedicamentoFactory extends Factory
{
    protected $model = HistorialMedicamento::class;

    public function definition()
    {
        return [
			'expediente_id' => $this->faker->name,
			'nombre' => $this->faker->name,
			'frecuencia_horaria' => $this->faker->name,
			'dosis' => $this->faker->name,
        ];
    }
}
