<?php

namespace Database\Factories;

use App\Models\HistorialSintoma;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class HistorialSintomaFactory extends Factory
{
    protected $model = HistorialSintoma::class;

    public function definition()
    {
        return [
			'expediente_id' => $this->faker->name,
			'nombre' => $this->faker->name,
			'antiguedad' => $this->faker->name,
			'hora' => $this->faker->name,
        ];
    }
}
