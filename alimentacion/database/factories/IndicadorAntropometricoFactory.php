<?php

namespace Database\Factories;

use App\Models\IndicadorAntropometrico;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class IndicadorAntropometricoFactory extends Factory
{
    protected $model = IndicadorAntropometrico::class;

    public function definition()
    {
        return [
			'nombre' => $this->faker->name,
        ];
    }
}
