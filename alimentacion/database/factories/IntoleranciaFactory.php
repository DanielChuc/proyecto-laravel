<?php

namespace Database\Factories;

use App\Models\Intolerancia;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class IntoleranciaFactory extends Factory
{
    protected $model = Intolerancia::class;

    public function definition()
    {
        return [
			'plan_id' => $this->faker->name,
			'alimento_id' => $this->faker->name,
        ];
    }
}
