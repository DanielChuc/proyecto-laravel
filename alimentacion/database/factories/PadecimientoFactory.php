<?php

namespace Database\Factories;

use App\Models\Padecimiento;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PadecimientoFactory extends Factory
{
    protected $model = Padecimiento::class;

    public function definition()
    {
        return [
			'nombre' => $this->faker->name,
			'descripcion' => $this->faker->name,
        ];
    }
}
