<?php

namespace Database\Factories;

use App\Models\PlanAlimentacion;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PlanAlimentacionFactory extends Factory
{
    protected $model = PlanAlimentacion::class;

    public function definition()
    {
        return [
			'expediente_id' => $this->faker->name,
			'fecha_inicio' => $this->faker->name,
			'fecha_fin' => $this->faker->name,
			'objetivo' => $this->faker->name,
			'total_diario' => $this->faker->name,
        ];
    }
}
