<?php

namespace Database\Factories;

use App\Models\Socioeconomico;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class SocioeconomicoFactory extends Factory
{
    protected $model = Socioeconomico::class;

    public function definition()
    {
        return [
			'expediente_id' => $this->faker->name,
			'presupuesto' => $this->faker->name,
			'utensilios' => $this->faker->name,
        ];
    }
}
