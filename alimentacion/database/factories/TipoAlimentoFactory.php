<?php

namespace Database\Factories;

use App\Models\TipoAlimento;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TipoAlimentoFactory extends Factory
{
    protected $model = TipoAlimento::class;

    public function definition()
    {
        return [
			'tipoalimento' => $this->faker->name,
        ];
    }
}
