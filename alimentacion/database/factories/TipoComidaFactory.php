<?php

namespace Database\Factories;

use App\Models\TipoComida;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TipoComidaFactory extends Factory
{
    protected $model = TipoComida::class;

    public function definition()
    {
        return [
			'tipocomida' => $this->faker->name,
        ];
    }
}
