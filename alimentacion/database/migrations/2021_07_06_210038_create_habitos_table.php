<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHabitosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('habitos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('expediente_id')->unsigned();
            $table->integer('cant_cigarro');
            $table->text('act_fisica');
            $table->string('consumo_agua');
            $table->boolean('azucar')->default(0);
            $table->boolean('sal')->default(0);
            // $table->enum('azucar', ['True', 'False']);
            // $table->enum('sal', ['True', 'False']);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('expediente_id')->references('id')->on('expedientes')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('habitos');
    }
}
