<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanAlimentacionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_alimentacions', function (Blueprint $table) {
            $table->id('id');
            $table->integer('expediente_id')->unsigned();
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->text('objetivo');
            $table->string('total_diario');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('expediente_id')->references('id')->on('expedientes')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan_alimentacions');
    }
}
