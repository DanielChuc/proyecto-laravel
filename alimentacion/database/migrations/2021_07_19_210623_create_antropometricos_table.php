<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAntropometricosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antropometricos', function (Blueprint $table) {
            $table->id('id');

            $table->unsignedBigInteger('plan_id');
            $table->unsignedBigInteger('indicador_id');
            $table->decimal('valor', 5, 2);
            $table->foreign('plan_id')->references('id')->on('plan_alimentacions')->onDelete('cascade');
            $table->foreign('indicador_id')->references('id')->on('indicador_antropometricos')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('antropometricos');
    }
}
