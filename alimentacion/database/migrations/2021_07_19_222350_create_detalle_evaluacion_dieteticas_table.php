<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleEvaluacionDieteticasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_evaluacion_dieteticas', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedBigInteger('evaluacion_id');
            $table->enum('tiempo', ['Desayuno', 'Comida', 'Cena', 'Colacion']);
            $table->string('hora');
            $table->string('lugar');
            $table->integer('duracion');
            $table->foreign('evaluacion_id')->references('id')->on('evaluacion_dieteticas')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalle_evaluacion_dieteticas');
    }
}
