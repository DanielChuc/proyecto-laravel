<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alimentos', function (Blueprint $table) {
            $table->id('id');
            $table->string('nombre');
            $table->unsignedBigInteger('tipoalimento_id');
            $table->string('descripcion')->nullable();
            $table->string('ingredientes')->nullable();
            $table->string('preparacion')->nullable();
            $table->decimal('calorias', 10, 0);
            $table->string('urlvideo')->nullable();
            $table->string('urlfotos')->nullable();
            $table->foreign('tipoalimento_id')->references('id')->on('tipo_alimentos')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alimentos');
    }
}
