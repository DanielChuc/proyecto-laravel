<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrecuenciaAlimentariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frecuencia_alimentarias', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedBigInteger('plan_id');
            $table->unsignedBigInteger('alimento_id');
            $table->integer('dias_semana');
            $table->foreign('plan_id')->references('id')->on('plan_alimentacions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('alimento_id')->references('id')->on('alimentos')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('frecuencia_alimentarias');
    }
}
