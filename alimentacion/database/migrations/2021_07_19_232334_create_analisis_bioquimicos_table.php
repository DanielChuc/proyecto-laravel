<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnalisisBioquimicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analisis_bioquimicos', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedBigInteger('plan_id');
            $table->datetime('fecha');
            $table->foreign('plan_id')->references('id')->on('plan_alimentacions')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('analisis_bioquimicos');
    }
}
