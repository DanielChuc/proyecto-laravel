<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleAnalisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_analises', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedBigInteger('analisis_id');
            $table->integer('indicador');
            $table->decimal('resultado', 5, 2);
            $table->foreign('analisis_id')->references('id')->on('analisis_bioquimicos')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalle_analises');
    }
}
