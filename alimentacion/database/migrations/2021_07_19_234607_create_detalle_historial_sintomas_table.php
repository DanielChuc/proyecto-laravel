<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleHistorialSintomasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_historial_sintomas', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedBigInteger('alimento_id');
            $table->integer('historial_sintoma_id')->unsigned();
            $table->foreign('alimento_id')->references('id')->on('alimentos')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('historial_sintoma_id')->references('id')->on('historial_sintomas')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalle_historial_sintomas');
    }
}
