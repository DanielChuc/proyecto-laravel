<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetallePlanAlimentacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_plan_alimentacions', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedBigInteger('plan_id');
            $table->unsignedBigInteger('tipocomida_id');
            $table->unsignedBigInteger('alimento_id');
            $table->decimal('cantidad', 10, 0);
            $table->string('unidad_medida');
            $table->string('nota');
            $table->string('dia_semana');
            $table->foreign('plan_id')->references('id')->on('plan_alimentacions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tipocomida_id')->references('id')->on('tipo_comidas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('alimento_id')->references('id')->on('alimentos')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalle_plan_alimentacions');
    }
}
