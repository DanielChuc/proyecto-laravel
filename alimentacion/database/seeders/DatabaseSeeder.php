<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(49)->create();
        \App\Models\Expediente::factory(50)->create();
        \App\Models\Antecedente::factory(50)->create();
        //terminar

    }
}
