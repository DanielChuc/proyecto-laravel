<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

<title>@hasSection('title') @yield('title') | @endif {{ config('app.name', 'Laravel') }}
</title>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
@livewireStyles
</head>

<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/admin/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                @auth()
                    <ul class="navbar-nav mr-auto">
                        <!--Nav Bar Hooks - Do not delete!!-->




                        <div class="dropdown show">
                            <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Segunda parte cruds
                            </a>


                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <li class="nav-item">
                                    <a href="{{ url('/admin/detalle_plan_alimentacions') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Detalle_plan_alimentacions</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/tipo_comidas') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Tipo_comidas</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/detalle_historial_sintomas') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Detalle_historial_sintomas</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/detalle_analises') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Detalle_analises</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/analisis_bioquimicos') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Analisis_bioquimicos</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/frecuencia_alimentarias') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Frecuencia_alimentarias</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/alergias') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Alergias</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/intolerancias') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Intolerancias</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/alimentos') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Alimentos</a>
                                </li>
                            </div>
                        </div>






                        <div class="btn-group dropright">
                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                Cruds
                            </button>
                            <div class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="{{ url('/admin/tipo_alimentos') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Tipo_alimentos</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/detalle_evaluacion_dieteticas') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Detalle_evaluacion_dieteticas</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/evaluacion_dieteticas') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Evaluacion_dieteticas</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/antropometricos') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Antropometricos</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/indicador_antropometricos') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Indicador_antropometricos</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/plan_alimentacions') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Plan_alimentacions</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/alergia_medicamentos') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Alergia_medicamentos</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/historial_medicamentos') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Historial_medicamentos</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/historial_sintomas') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Historial_sintomas</a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ url('/admin/habitos') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Habitos</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/socioeconomicos') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Socioeconomicos</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/antecedentepadecimientos') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Antecedentepadecimientos</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/padecimientos') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Padecimientos</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/antecedentes') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Antecedentes</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/admin/expedientes') }}" class="nav-link"><i
                                            class="fab fa-laravel text-info"></i> Expedientes</a>
                                </li>
                            </div>
                        </div>




                    </ul>
                @endauth()

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-2">
        @yield('content')
    </main>
</div>
@livewireScripts
<script type="text/javascript">
    window.livewire.on('closeModal', () => {
        $('#exampleModal').modal('hide');
    });
</script>
</body>

</html>
