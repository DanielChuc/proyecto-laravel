<!-- Modal -->
<div wire:ignore.self class="modal fade" id="exampleModal" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear un nuevo Alimento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="nombre">Nombre:</label>
                        <input wire:model="nombre" type="text" class="form-control" id="nombre"
                            placeholder="Nombre">@error('nombre') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="tipoalimento_id">Tipo alimento ID:</label>
                        <input wire:model="tipoalimento_id" type="number" class="form-control" id="tipoalimento_id"
                            placeholder="Tipoalimento Id">@error('tipoalimento_id') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripción:</label>
                        <input wire:model="descripcion" type="text" class="form-control" id="descripcion"
                            placeholder="Descripcion">@error('descripcion') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="ingredientes">Ingredientes:</label>
                        <input wire:model="ingredientes" type="text" class="form-control" id="ingredientes"
                            placeholder="Ingredientes">@error('ingredientes') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="preparacion">Preparación:</label>
                        <input wire:model="preparacion" type="text" class="form-control" id="preparacion"
                            placeholder="Preparacion">@error('preparacion') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="calorias">Calorias:</label>
                        <input wire:model="calorias" type="number" step="any" class="form-control" id="calorias"
                            placeholder="Calorias">@error('calorias') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="urlvideo">Url Video:</label>
                        <input wire:model="urlvideo" type="text" class="form-control" id="urlvideo"
                            placeholder="Urlvideo">@error('urlvideo') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="urlfotos">Url Fotos:</label>
                        <input wire:model="urlfotos" type="text" class="form-control" id="urlfotos"
                            placeholder="Urlfotos">@error('urlfotos') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Cerrar</button>
                <button type="button" wire:click.prevent="store()" class="btn btn-primary close-modal">Guardar</button>
            </div>
        </div>
    </div>
</div>
