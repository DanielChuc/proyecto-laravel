<!-- Modal -->
<div wire:ignore.self class="modal fade" id="exampleModal" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear un nuevo Detalle Evaluacion Dietetica</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="evaluacion_id">Evaluación ID:</label>
                        <input wire:model="evaluacion_id" type="number" class="form-control" id="evaluacion_id"
                            placeholder="Evaluacion Id">@error('evaluacion_id') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">

                        <label for="tiempo">Tiempo:</label>
                        <select class="form-control" wire:model="tiempo" class="form-control" id="tiempo">
                            <option value="#">-- Seleciona uno --</option>
                            <option value="Desayuno">Desayuno</option>
                            <option value="Comida">Comida</option>
                            <option value="Cena">Cena</option>
                            <option value="Colacion">Colacion</option>
                        </select>
                        @error('tiempo') <span class="error text-danger">{{ $message }}</span> @enderror

                        {{-- <input  type="text" 
                            placeholder="Tiempo"> --}}
                    </div>
                    <div class="form-group">
                        <label for="hora">Hora:</label>
                        <input wire:model="hora" type="time" class="form-control" id="hora"
                            placeholder="Hora">@error('hora') <span class="error text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="lugar">Lugar:</label>
                        <input wire:model="lugar" type="text" class="form-control" id="lugar"
                            placeholder="Lugar">@error('lugar') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="duracion">Duración:</label>
                        <input wire:model="duracion" type="number" class="form-control" id="duracion"
                            placeholder="Duracion">@error('duracion') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Cerrar</button>
                <button type="button" wire:click.prevent="store()" class="btn btn-primary close-modal">Guardar</button>
            </div>
        </div>
    </div>
</div>
