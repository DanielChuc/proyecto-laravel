@section('title', __('Detalle Evaluacion Dieteticas'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">
						<div class="float-left">
							<h4><i class="fab fa-laravel text-info"></i>
							Detalle Evaluacion Dietetica Listing </h4>
						</div>
						{{--<div wire:poll.60s>
							<code><h5>{{ now()->format('H:i:s') }} UTC</h5></code>
						</div>--}}
						@if (session()->has('message'))
						<div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
						@endif
						<div>
							<input wire:model='keyWord' type="text" class="form-control" name="search" id="search" placeholder="Buscar Detalle Evaluacion Dieteticas">
						</div>
						<div class="btn btn-sm btn-info" data-toggle="modal" data-target="#exampleModal">
						<i class="fa fa-plus"></i>  Añadir Detalle Evaluacion Dieteticas
						</div>
					</div>
				</div>
				
				<div class="card-body">
						@include('livewire.detalle-evaluacion-dieteticas.create')
						@include('livewire.detalle-evaluacion-dieteticas.update')
				<div class="table-responsive">
					<table class="table table-striped table-sm">
						<thead class="thead">
							<tr> 
								<td>#</td> 
								<th>Evaluacion Id</th>
								<th>Tiempo</th>
								<th>Hora</th>
								<th>Lugar</th>
								<th>Duracion</th>
								<td class="text-danger"><strong>Acciones</strong> </td>
							</tr>
						</thead>
						<tbody>
							@foreach($detalleEvaluacionDieteticas as $row)
							<tr>
								<td>{{ $loop->iteration }}</td> 
								<td>{{ $row->evaluacion_id }}</td>
								<td>{{ $row->tiempo }}</td>
								<td>{{ $row->hora }}</td>
								<td>{{ $row->lugar }}</td>
								<td>{{ $row->duracion }}</td>
								<td width="90">
								<div class="btn-group">
									<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Acciones
									</button>
									<div class="dropdown-menu dropdown-menu-right">
									<a data-toggle="modal" data-target="#updateModal" class="dropdown-item" wire:click="edit({{$row->id}})"><i class="fa fa-edit"></i> Editar </a>							 
									<a class="dropdown-item" onclick="confirm('¿Confirmar eliminación de Detalle Evaluacion Dietetica id {{$row->id}}?  \nUna vez Eliminado  Detalle Evaluacion Dieteticas   No puede ser recuperado!')||event.stopImmediatePropagation()" wire:click="destroy({{$row->id}})"><i class="fa fa-trash"></i> Eliminar </a>   
									</div>
								</div>
								</td>
							@endforeach
						</tbody>
					</table>						
					{{ $detalleEvaluacionDieteticas->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>