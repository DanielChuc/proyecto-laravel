<!-- Modal -->
<div wire:ignore.self class="modal fade" id="exampleModal" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear un nuevo Detalle Plan Alimentacion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="plan_id">Plan ID:</label>
                        <input wire:model="plan_id" type="number" class="form-control" id="plan_id"
                            placeholder="Plan Id">@error('plan_id') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="tipocomida_id">Tipo comida ID:</label>
                        <input wire:model="tipocomida_id" type="number" class="form-control" id="tipocomida_id"
                            placeholder="Tipocomida Id">@error('tipocomida_id') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="alimento_id">Alimento ID:</label>
                        <input wire:model="alimento_id" type="number" class="form-control" id="alimento_id"
                            placeholder="Alimento Id">@error('alimento_id') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="cantidad">Cantidad:</label>
                        <input wire:model="cantidad" type="number" step="any" class="form-control" id="cantidad"
                            placeholder="Cantidad">@error('cantidad') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="unidad_medida">Unidad Medida:</label>
                        <input wire:model="unidad_medida" type="text" class="form-control" id="unidad_medida"
                            placeholder="Unidad Medida">@error('unidad_medida') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="nota">Nota:</label>
                        <input wire:model="nota" type="text" class="form-control" id="nota"
                            placeholder="Nota">@error('nota') <span class="error text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="dia_semana">Dias de la semana:</label>
                        <input wire:model="dia_semana" type="text" class="form-control" id="dia_semana"
                            placeholder="Dia Semana">@error('dia_semana') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Cerrar</button>
                <button type="button" wire:click.prevent="store()" class="btn btn-primary close-modal">Guardar</button>
            </div>
        </div>
    </div>
</div>
