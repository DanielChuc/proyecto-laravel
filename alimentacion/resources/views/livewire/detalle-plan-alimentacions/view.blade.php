@section('title', __('Detalle Plan Alimentacions'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">
						<div class="float-left">
							<h4><i class="fab fa-laravel text-info"></i>
							Detalle Plan Alimentacion Listing </h4>
						</div>
						{{--<div wire:poll.60s>
							<code><h5>{{ now()->format('H:i:s') }} UTC</h5></code>
						</div>--}}
						@if (session()->has('message'))
						<div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
						@endif
						<div>
							<input wire:model='keyWord' type="text" class="form-control" name="search" id="search" placeholder="Buscar Detalle Plan Alimentacions">
						</div>
						<div class="btn btn-sm btn-info" data-toggle="modal" data-target="#exampleModal">
						<i class="fa fa-plus"></i>  Añadir Detalle Plan Alimentacions
						</div>
					</div>
				</div>
				
				<div class="card-body">
						@include('livewire.detalle-plan-alimentacions.create')
						@include('livewire.detalle-plan-alimentacions.update')
				<div class="table-responsive">
					<table class="table table-striped table-sm">
						<thead class="thead">
							<tr> 
								<td>#</td> 
								<th>Plan Id</th>
								<th>Tipocomida Id</th>
								<th>Alimento Id</th>
								<th>Cantidad</th>
								<th>Unidad Medida</th>
								<th>Nota</th>
								<th>Dia Semana</th>
								<td class="text-danger"><strong>Acciones</strong> </td>
							</tr>
						</thead>
						<tbody>
							@foreach($detallePlanAlimentacions as $row)
							<tr>
								<td>{{ $loop->iteration }}</td> 
								<td>{{ $row->plan_id }}</td>
								<td>{{ $row->tipocomida_id }}</td>
								<td>{{ $row->alimento_id }}</td>
								<td>{{ $row->cantidad }}</td>
								<td>{{ $row->unidad_medida }}</td>
								<td>{{ $row->nota }}</td>
								<td>{{ $row->dia_semana }}</td>
								<td width="90">
								<div class="btn-group">
									<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Acciones
									</button>
									<div class="dropdown-menu dropdown-menu-right">
									<a data-toggle="modal" data-target="#updateModal" class="dropdown-item" wire:click="edit({{$row->id}})"><i class="fa fa-edit"></i> Editar </a>							 
									<a class="dropdown-item" onclick="confirm('¿Confirmar eliminación de Detalle Plan Alimentacion id {{$row->id}}?  \nUna vez Eliminado  Detalle Plan Alimentacions   No puede ser recuperado!')||event.stopImmediatePropagation()" wire:click="destroy({{$row->id}})"><i class="fa fa-trash"></i> Eliminar </a>   
									</div>
								</div>
								</td>
							@endforeach
						</tbody>
					</table>						
					{{ $detallePlanAlimentacions->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>