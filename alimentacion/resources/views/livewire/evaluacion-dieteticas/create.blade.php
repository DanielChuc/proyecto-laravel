<!-- Modal -->
<div wire:ignore.self class="modal fade" id="exampleModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear un nuevo Evaluacion Dietetica</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
           <div class="modal-body">
				<form>
            <div class="form-group">
                <label for="plan_id">Plan ID:</label>
                <input wire:model="plan_id" type="text" class="form-control" id="plan_id" placeholder="Plan Id">@error('plan_id') <span class="error text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="hora_apetito">Hora apetito:</label>
                <input wire:model="hora_apetito" type="time" class="form-control" id="hora_apetito" placeholder="Hora Apetito">@error('hora_apetito') <span class="error text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="saciado">Saciado:</label>
                <input wire:model="saciado" type="number" min="0" max="1" class="form-control" id="saciado" placeholder="Saciado">@error('saciado') <span class="error text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="quien_cocina">¿Quien cocina?:</label>
                <input wire:model="quien_cocina" type="text" class="form-control" id="quien_cocina" placeholder="Quien Cocina">@error('quien_cocina') <span class="error text-danger">{{ $message }}</span> @enderror
            </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Cerrar</button>
                <button type="button" wire:click.prevent="store()" class="btn btn-primary close-modal">Guardar</button>
            </div>
        </div>
    </div>
</div>