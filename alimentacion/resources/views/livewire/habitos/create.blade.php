<!-- Modal -->
<div wire:ignore.self class="modal fade" id="exampleModal" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear un nuevo Habito</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="expediente_id">Expediente ID:</label>
                        <input wire:model="expediente_id" type="number" class="form-control" id="expediente_id"
                            placeholder="Expediente Id">@error('expediente_id') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="cant_cigarro">Cantidad de cigarros:</label>
                        <input wire:model="cant_cigarro" type="number" class="form-control" id="cant_cigarro"
                            placeholder="Cant Cigarro">@error('cant_cigarro') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="act_fisica">Actividad física: </label>
                        <input wire:model="act_fisica" type="text" class="form-control" id="act_fisica"
                            placeholder="Act Fisica">@error('act_fisica') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="consumo_agua">Consumo de agua:</label>
                        <input wire:model="consumo_agua" type="text" class="form-control" id="consumo_agua"
                            placeholder="Consumo Agua">@error('consumo_agua') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="azucar">Azúcar:</label>
                        <input wire:model="azucar" type="number" min="0"  max="1" class="form-control" id="azucar"
                            placeholder="Azucar">@error('azucar') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="sal">Sal:</label>
                        <input wire:model="sal" type="number" min="0" max="1" class="form-control" id="sal"
                            placeholder="Sal">@error('sal') <span class="error text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Cerrar</button>
                <button type="button" wire:click.prevent="store()" class="btn btn-primary close-modal">Guardar</button>
            </div>
        </div>
    </div>
</div>
