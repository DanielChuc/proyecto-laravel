@section('title', __('Habitos'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">
						<div class="float-left">
							<h4><i class="fab fa-laravel text-info"></i>
							Habito Listing </h4>
						</div>
						{{--<div wire:poll.60s>
							<code><h5>{{ now()->format('H:i:s') }} UTC</h5></code>
						</div>--}}
						@if (session()->has('message'))
						<div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
						@endif
						<div>
							<input wire:model='keyWord' type="text" class="form-control" name="search" id="search" placeholder="Buscar Habitos">
						</div>
						<div class="btn btn-sm btn-info" data-toggle="modal" data-target="#exampleModal">
						<i class="fa fa-plus"></i>  Añadir Habitos
						</div>
					</div>
				</div>
				
				<div class="card-body">
						@include('livewire.habitos.create')
						@include('livewire.habitos.update')
				<div class="table-responsive">
					<table class="table table-striped table-sm">
						<thead class="thead">
							<tr> 
								<td>#</td> 
								<th>Expediente Id</th>
								<th>Cantidad de cigarros</th>
								<th>Actividad Física</th>
								<th>Consumo de Agua</th>
								<th>Azúcar</th>
								<th>Sal</th>
								<td class="text-danger"><strong>Acciones</strong> </td>
							</tr>
						</thead>
						<tbody>
							@foreach($habitos as $row)
							<tr>
								<td>{{ $loop->iteration }}</td> 
								<td>{{ $row->expediente_id }}</td>
								<td>{{ $row->cant_cigarro }}</td>
								<td>{{ $row->act_fisica }}</td>
								<td>{{ $row->consumo_agua }}</td>
								<td>{{ $row->azucar }}</td>
								<td>{{ $row->sal }}</td>
								<td width="90">
								<div class="btn-group">
									<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Acciones
									</button>
									<div class="dropdown-menu dropdown-menu-right">
									<a data-toggle="modal" data-target="#updateModal" class="dropdown-item" wire:click="edit({{$row->id}})"><i class="fa fa-edit"></i> Editar </a>							 
									<a class="dropdown-item" onclick="confirm('¿Confirmar eliminación de Habito id {{$row->id}}?  \nUna vez Eliminado  Habitos   No puede ser recuperado!')||event.stopImmediatePropagation()" wire:click="destroy({{$row->id}})"><i class="fa fa-trash"></i> Eliminar </a>   
									</div>
								</div>
								</td>
							@endforeach
						</tbody>
					</table>						
					{{ $habitos->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>