<!-- Modal -->
<div wire:ignore.self class="modal fade" id="updateModal" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Actualizar  Plan Alimentacion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span wire:click.prevent="cancel()" aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" wire:model="selected_id">
                    <div class="form-group">
                        <label for="expediente_id">Expediente ID:</label>
                        <input wire:model="expediente_id" type="text" class="form-control" id="expediente_id"
                            placeholder="Expediente Id">@error('expediente_id') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="fecha_inicio">Fecha inicio:</label>
                        <input wire:model="fecha_inicio" type="text" class="form-control" id="fecha_inicio"
                            placeholder="Fecha Inicio">@error('fecha_inicio') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="fecha_fin">Fecha fin:</label>
                        <input wire:model="fecha_fin" type="text" class="form-control" id="fecha_fin"
                            placeholder="Fecha Fin">@error('fecha_fin') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="objetivo">Objetivo:</label>
                        <textarea wire:model="objetivo" class="form-control" id="objetivo" cols="30"
                            rows="10"></textarea>@error('objetivo') <span
                            class="error text-danger">{{ $message }}</span> @enderror

                    </div>
                    <div class="form-group">
                        <label for="total_diario">Total diario:</label>
                        <input wire:model="total_diario" type="text" class="form-control" id="total_diario"
                            placeholder="Total Diario">@error('total_diario') <span
                            class="error text-danger">{{ $message }}</span> @enderror
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary"
                    data-dismiss="modal">Cerrar</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary"
                    data-dismiss="modal">Guardar</button>
            </div>
        </div>
    </div>
</div>
