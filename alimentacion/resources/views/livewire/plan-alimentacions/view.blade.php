@section('title', __('Plan Alimentacions'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">
						<div class="float-left">
							<h4><i class="fab fa-laravel text-info"></i>
							Plan Alimentacion Listing </h4>
						</div>
						{{--<div wire:poll.60s>
							<code><h5>{{ now()->format('H:i:s') }} UTC</h5></code>
						</div>--}}
						@if (session()->has('message'))
						<div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
						@endif
						<div>
							<input wire:model='keyWord' type="text" class="form-control" name="search" id="search" placeholder="Buscar Plan Alimentacions">
						</div>
						<div class="btn btn-sm btn-info" data-toggle="modal" data-target="#exampleModal">
						<i class="fa fa-plus"></i>  Añadir Plan Alimentacions
						</div>
					</div>
				</div>
				
				<div class="card-body">
						@include('livewire.plan-alimentacions.create')
						@include('livewire.plan-alimentacions.update')
				<div class="table-responsive">
					<table class="table table-striped table-sm">
						<thead class="thead">
							<tr> 
								<td>#</td> 
								<th>Expediente Id</th>
								<th>Fecha Inicio</th>
								<th>Fecha Fin</th>
								<th>Objetivo</th>
								<th>Total Diario</th>
								<td class="text-danger"><strong>Acciones</strong> </td>
							</tr>
						</thead>
						<tbody>
							@foreach($planAlimentacions as $row)
							<tr>
								<td>{{ $loop->iteration }}</td> 
								<td>{{ $row->expediente_id }}</td>
								<td>{{ $row->fecha_inicio }}</td>
								<td>{{ $row->fecha_fin }}</td>
								<td>{{ $row->objetivo }}</td>
								<td>{{ $row->total_diario }}</td>
								<td width="90">
								<div class="btn-group">
									<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Acciones
									</button>
									<div class="dropdown-menu dropdown-menu-right">
									<a data-toggle="modal" data-target="#updateModal" class="dropdown-item" wire:click="edit({{$row->id}})"><i class="fa fa-edit"></i> Editar </a>							 
									<a class="dropdown-item" onclick="confirm('¿Confirmar eliminación de Plan Alimentacion id {{$row->id}}?  \nUna vez Eliminado  Plan Alimentacions   No puede ser recuperado!')||event.stopImmediatePropagation()" wire:click="destroy({{$row->id}})"><i class="fa fa-trash"></i> Eliminar </a>   
									</div>
								</div>
								</td>
							@endforeach
						</tbody>
					</table>						
					{{ $planAlimentacions->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>