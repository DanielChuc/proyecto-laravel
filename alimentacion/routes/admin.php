<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('', [HomeController::class,'index']);

//Route Hooks - Do not delete//
	Route::view('detalle_plan_alimentacions', 'livewire.detalle-plan-alimentacions.index')->middleware('auth');
	Route::view('tipo_comidas', 'livewire.tipo-comidas.index')->middleware('auth');
	Route::view('detalle_historial_sintomas', 'livewire.detalle-historial-sintomas.index')->middleware('auth');
	Route::view('detalle_analises', 'livewire.detalle-analises.index')->middleware('auth');
	Route::view('analisis_bioquimicos', 'livewire.analisis-bioquimicos.index')->middleware('auth');
	Route::view('frecuencia_alimentarias', 'livewire.frecuencia-alimentarias.index')->middleware('auth');
	Route::view('alergias', 'livewire.alergias.index')->middleware('auth');
	Route::view('intolerancias', 'livewire.intolerancias.index')->middleware('auth');
	Route::view('alimentos', 'livewire.alimentos.index')->middleware('auth');
	Route::view('tipo_alimentos', 'livewire.tipo-alimentos.index')->middleware('auth');
	Route::view('detalle_evaluacion_dieteticas', 'livewire.detalle-evaluacion-dieteticas.index')->middleware('auth');
	Route::view('evaluacion_dieteticas', 'livewire.evaluacion-dieteticas.index')->middleware('auth');
	Route::view('antropometricos', 'livewire.antropometricos.index')->middleware('auth');
	Route::view('indicador_antropometricos', 'livewire.indicador-antropometricos.index')->middleware('auth');
	Route::view('plan_alimentacions', 'livewire.plan-alimentacions.index')->middleware('auth');
	Route::view('alergia_medicamentos', 'livewire.alergia-medicamentos.index')->middleware('auth');
	Route::view('historial_medicamentos', 'livewire.historial-medicamentos.index')->middleware('auth');
	Route::view('historial_sintomas', 'livewire.historial-sintomas.index')->middleware('auth');
	Route::view('habitos', 'livewire.habitos.index')->middleware('auth');
	Route::view('socioeconomicos', 'livewire.socioeconomicos.index')->middleware('auth');
	Route::view('antecedentepadecimientos', 'livewire.antecedentepadecimientos.index')->middleware('auth');
	Route::view('padecimientos', 'livewire.padecimientos.index')->middleware('auth');
	Route::view('antecedentes', 'livewire.antecedentes.index')->middleware('auth');
	Route::view('expedientes', 'livewire.expedientes.index')->middleware('auth');
